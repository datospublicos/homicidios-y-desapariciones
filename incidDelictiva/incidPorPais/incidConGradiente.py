#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

archData = 'incidDelict.csv'

#data = pd.read_csv(archData, encoding = 'ISO-8859-1')
data = pd.read_csv(archData)
#data = data.fillna(0)

#print(data.iloc[0:33, [0, 1, 32, 33, 34, 35]])

daAnhoPais = data[['Año', 'Total']].copy()
#print(daAnhoPais)

daAnhoPais['dx'] = daAnhoPais['Año'].diff().fillna(0)
daAnhoPais['dy'] = 5 * daAnhoPais['Total'].diff() #.fillna(0)
#data['dy/dx'] = (df['dy'] / df['dx']).fillna(0)
print(daAnhoPais)

mpl.style.use('default')
plt_1 = plt.figure(figsize=(10, 4))
plt.plot(daAnhoPais['Año'], daAnhoPais['Total'], )
plt.stem( daAnhoPais['Año'], daAnhoPais['dy'], linefmt = 'C1', markerfmt = 'C1o')
plt.legend(['Incidencia delictiva por año', 'Tasa de crecimiento o decrecimiento por año (multiplicada por 5)'], loc = 'lower left')
plt.fill_between(data['Año'], 0, daAnhoPais['Total'], alpha = 0.3)
plt.xlabel('Año')
plt.ylabel('Incidencia delictiva')
plt.title('Incidencia delictiva por cada 100 mil hab. en México')
plt.grid()
plt.savefig('incidDelictivaMexico.png')
plt.show()

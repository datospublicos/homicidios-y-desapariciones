#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
import bar_chart_race as bcr

archData = 'incidDelict.csv'
archVideo = 'sal01c.mp4'

data = pd.read_csv(archData,
    #encoding = 'ISO-8859-1',
    index_col = 0,
    na_values = ['_', 'D']
)
#data = data.fillna(0)
data = data.drop(['Total'], axis = 1)

#print(data.iloc[0:32, [0, 9, 10, 11, 12, 13, 14, 15, 16]])
#print(data)

df = data
bcr.bar_chart_race(df,
    n_bars = 11,
    filename = archVideo,
    fixed_max = False,
    steps_per_period = 25,
    period_length = 5000,
    title = 'Incidencia delictiva por cada 100 mil hab. en México',
    filter_column_colors = True,
)

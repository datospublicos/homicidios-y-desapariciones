#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

archData = 'homicidios03.csv'

data = pd.read_csv(archData, encoding = 'ISO-8859-1')
data = data.fillna(0)

#print(data.iloc[0:33, [0, 1, 32, 33, 34, 35]])

daAnhoPais = data[['Año', 'Total']].copy()
#print(daAnhoPais)

daAnhoPais['dx'] = daAnhoPais['Año'].diff() #.fillna(0)
daAnhoPais['dy'] = daAnhoPais['Total'].diff() #.fillna(0)
#data['dy/dx'] = (df['dy'] / df['dx']).fillna(0)
print(daAnhoPais)

mpl.style.use('default')
plt_1 = plt.figure(figsize=(10, 4))
plt.plot(daAnhoPais['Año'], daAnhoPais['Total'], )
plt.stem( daAnhoPais['Año'], daAnhoPais['dy'], linefmt = 'C1', markerfmt = 'C1o')
plt.legend(['Homicidios dolosos por año', 'Tasa de crecimiento o decrecimiento por año'], loc = 'upper left')
plt.fill_between(data['Año'], 0, daAnhoPais['Total'], alpha = 0.3)
plt.xlabel('Año')
plt.ylabel('Homicidios dolosos')
plt.title('Homicidios dolosos en México')
plt.grid()
plt.savefig('homDolososMexico.png')
plt.show()

#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd

archData = 'homicidios03.csv'
#archVideo = 'prueba02.mp4'

#data = pd.read_csv(archData, delim_whitespace = True,
#    index_col = 0, na_values = ['_', 'D'],
#    infer_datetime_format = 1,
#)

data = pd.read_csv(archData, encoding = 'ISO-8859-1')
data = data.fillna(0)

#print(data.iloc[0:33, [0, 1, 32, 33, 34, 35]])
print(data)


daAnhoPais = data[['Año', 'Total']].copy()
print(daAnhoPais)

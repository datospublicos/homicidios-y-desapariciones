#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
import bar_chart_race as bcr

archData = 'desapPorEdo-2000a2023.dat'
archVideo = 'prueba02.mp4'

data = pd.read_csv(archData, delim_whitespace = True,
    index_col = 0, na_values = ['_', 'D'],
    infer_datetime_format = 1,
)
#data = data.apply(pd.to_numeric, errors = 'coerce', axis = 1)
#data = data.fillna(0).T

#print(data)
tamFig = (5, 3)

df = data
bcr.bar_chart_race(df,
    #figsize = tamFig,
    n_bars = 11,
    filename = archVideo,
    fixed_max = True,
    steps_per_period = 20,
    period_length = 3000,
    title = 'Personas desaparecidas en México',
    filter_column_colors = True,
    #interpolate_period = True,
    #period_fmt = '%Y',
)

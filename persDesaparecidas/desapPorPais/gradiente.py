#!/usr/bin/python3
# -*- using: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, FixedLocator
import matplotlib as mpl

archData = 'desapPais-1990a2023.dat'
data = pd.read_csv(archData, delim_whitespace = True)

data['dx'] = data['Año'].diff() #.fillna(0)
data['dy'] = data['Personas desaparecidas'].diff() #.fillna(0)
#data['dy/dx'] = (df['dy'] / df['dx']).fillna(0)
print(data)

mpl.style.use('default')
plt_1 = plt.figure(figsize=(10, 4))
plt.plot(data['Año'], data['Personas desaparecidas'], )
plt.stem( data['Año'], data['dy'], linefmt = 'C1', markerfmt = 'C1o')
plt.legend(['Personas desaparecidas por año', 'Tasa de crecimiento o decrecimiento por año'], loc = 'upper left')
plt.fill_between(data['Año'], 0, data['Personas desaparecidas'], alpha = 0.3)
plt.xlabel('Año')
plt.ylabel('Personas desaparecidas')
plt.title('Personas desaparecidas en México')
plt.grid()
plt.savefig('persDesapPais.png')
plt.show()




# Ejemplo de diff
df = pd.DataFrame({'x': [1, 2, 4, 7, 11], 'y': [3, 5, 7, 9, 11]})
df['dx'] = df['x'].diff().fillna(0)
df['dy'] = df['y'].diff().fillna(0)
df['dy/dx'] = (df['dy'] / df['dx']).fillna(0)
#print(df)

## HOMICIDIOS DOLOSOS, PERSONAS DESAPARECIDAS, INCIDENCIA DELICTIVA EN MÉXICO

Enseguida se presentan gráficas sobre la evolución de las cifras en cuanto a homicidios dolosos y personas desaparecidas en México. Los datos fueron obtenidos de información pública del INEGI y del RNPDNO (Registro Nacional de Personas Desaparecidas y No Localizadas), este último recientemente dado a conocer. Los sitios son:

https://www.inegi.org.mx/sistemas/olap/proyectos/bd/continuas/mortalidad/defuncioneshom.asp?s=est

https://versionpublicarnpdno.segob.gob.mx/Dashboard/ContextoGeneral

Los datos para estas gráficas fueron recopilados entre el 8 y el 15 de septiembre de 2023.

Atte.: Braquistos

-----
### HOMICIDIOS DOLOSOS
La gráfica estática muestra los datos de todo el país por año en azul y la tasa de crecimiento o decrecimiento marcados como alfileres en color naranja. Los valores positivos de la tasa indican una tendencia a la alza, cuanto más grande sea su valor la tendencia es mayor, en tanto que valores negativos indican una tendencia a la baja. De esa forma, hasta la fecha en 2017 se ha presentado la mayor tendencia a la alza y en 2022 la mayor tendencia a la baja.

-----
### PERSONAS DESAPARECIDAS
La categorización en el sitio de RNPDNO es como "Personas desaparecidas", "No localizadas" y "Localizadas". Estas últimas se dividen en "Localizadas sin vida" y "Localizadas con vida". La diferencia entre "Personas desaparecidas" y "No localizadas" está en que en el primer caso se presume que la condición es como consecuencia de la comisión de un delito. Para más detalles consultar directamente en el sitio. Las gráficas sólo muestran el caso de "Personas desaparecidas".

La gráfica animada muestra la suma de hombres, mujeres y personas no identificadas por entidad federativa a lo largo de los años. Existe una cantidad de personas desaparecidas de las que no hay datos de fecha, en este caso la información se incorporó de manera proporcional a lo largo del tiempo por cada entidad federativa.

-----
<!-- ### PERSONAS DESAPARECIDAS -->
De la misma forma que en la gráfica estática de Homicidios dolosos, esta gráfica también muestra los datos por año de todo el país en azul y la tasa de crecimiento o decrecimiento en color naranja. La tendencia a la alza está dada por los valores positivos de la tasa, mientras que los valores negativos es una tendencia a la baja. Así, la mayor tendencia a la alza hasta el momento se presentó en 2010 en tanto que la mayor tendencia a la baja lo hizo en el año de 2020.

-----
### INCIDENCIA DELICTIVA
Gráficas de incidencia delictiva en México desde 2011 de acuerdo a la Encuesta Nacional de Victimización y Percepción sobre Seguridad Pública (ENVIPE) 2023 del INEGI. En la gráfica estática la tendencia a la alza o a la baja está denotada por la tasa de crecimiento o decrecimento mostrada por las marcas de color anaranjado. Valores positivos de la taza es tendencia a la alza y los negativos a la baja. La mayor tendencia a la alza se presentó en el año de 2013, en tanto que la mayor tendencia a la baja estuvo presente en el año de 2020.

Origen de los datos:

https://www.inegi.org.mx/programas/envipe/2023/


-----

#### Nota
La preparación de los archivos de datos fueron hechas con R y con Pandas de Python. Las gráficas animadas con Python y las gráficas estáticas con Matplotlib de Python.

-----
